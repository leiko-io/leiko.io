/* extern crate log;

use leiko_io::*;

// use storage::StorageAccess;
use storage::StorageAccessErrorKind;
use storage::mysql::MySqlAccessor;
use storage::manager::*;

use actix_web::{
    post, middleware, web, HttpServer, HttpResponse, Error as WebError, App
};
use actix::prelude::*;

use serde::Deserialize;

#[derive(Deserialize)]
struct RegisterUserRequest {
    username: String,
    password: String,
}

#[post("/register")]
async fn register_user(
    db: web::Data<Addr<StorageManager<MySqlAccessor>>>,
    register: web::Form<RegisterUserRequest>,
) -> Result<HttpResponse, WebError> {
    // check if user with username exists
    match db.send(GetUser::username(&register.username)).await? {
        Ok(_) => return Ok(HttpResponse::Conflict().finish()),
        Err(err) => match err.kind() {
            StorageAccessErrorKind::NotFound => (),
            _ => panic!(err),
        },
    }

    let id = db.send(RegisterUser::new(&register.username, &register.password))
        .await?.unwrap();

    Ok(HttpResponse::Ok().body(id.to_string()))
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    logger::init().unwrap();

    // Start 3 parallel db executors
    let addr = SyncArbiter::start(3, || {
        StorageManager::new(MySqlAccessor::new(
            "mysql://leiko:pcDXiPeb3sxUjzZRt9s2dbgBTLToQqqx@localhost/leiko",
        ).unwrap(), "public")
    });

    HttpServer::new(move || {
        App::new()
            .data(addr.clone())
            .wrap(middleware::Logger::default())
            .service(register_user)
    })
        .bind("127.0.0.1:8080")?
        .run()
        .await
} */

fn main() {}
