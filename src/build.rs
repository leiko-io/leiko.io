use git2::Repository;

fn main() {
    println!("cargo:rustc-env=COMPILE_GIT_HASH={}",
        match Repository::open(".") {
            Ok(repo) => repo.head().unwrap()
                .peel_to_commit().unwrap()
                .id().to_string(),
            Err(_) => {
                println!("cargo:warning=No git repository found! Versioning
                    may not work properly.");
                String::from("git_not_found")
            }
        });
}
