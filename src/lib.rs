#![feature(test)]
extern crate test;

extern crate chrono;
extern crate sha2;
extern crate rand;
extern crate rand_chacha;
extern crate log;
extern crate libc;
extern crate bzip2;

pub mod logger;
pub mod pass;
pub mod uid;
pub mod storage;

/// Stores the commit hash of the git HEAD.
///
/// When the build script is run, it fetches the git HEAD of the repository.
/// This is normally substituted in version requests and other places where
/// metadata is convienent, like the index page.
pub const COMPILE_GIT_HASH: &str = env!("COMPILE_GIT_HASH", "no git hash!");
