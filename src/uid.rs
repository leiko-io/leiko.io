use std::fmt::{Display, Formatter, Error as FmtError, Write};
use std::time::{SystemTime, UNIX_EPOCH};
use std::convert::TryFrom;

use rand::{RngCore};

/// Base64 conversion radix.
pub const BASE64_RADIX: &str = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-_";

const BASE64_U64_DIGITS: usize = ((std::mem::size_of::<u64>()*8) / 6) + 1;

/// Leiko.io epoch.
///
/// This instant in time is the first second of the first minute of the first
/// hour of the first day of the first month of 2020 in UTC. In `Rfc2822`, this
/// looks like this:
///
/// ```text
/// Wed, 1 Jan 2020 00:00:00 UTC
/// ```
pub const LEIKO_EPOCH: u128 = 1577836800000;

/// A generated uid.
///
/// This is an intermediary result that can be directly converted to an
/// internal integer, or a base64 number traditionally used in Uris and API
/// accesses.
///
/// To convert a `Uid` to type `T`, see `Uid`'s `Into<T>` implementation.
/// To convert a `T` type to `Uid`, see `Uid`'s `From<T>` implementation.
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Uid(u64);

impl Uid {
    /// Create a new uid from an internally stored `u64`.
    pub fn new(int: u64) -> Uid {
        Uid(int)
    }
}

impl Into<u64> for Uid {
    fn into(self) -> u64 {
        self.0
    }
}

impl From<u64> for Uid {
    fn from(u: u64) -> Uid {
        Uid::new(u)
    }
}

impl Display for Uid {
    fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError> {
        // trims the first zeros in a Uid
        let mut trim = true;

        // convert to base64
        for sh in 1..=BASE64_U64_DIGITS {
            // get the location
            let loc = (self.0 >> ((BASE64_U64_DIGITS-sh)*6)) & 0x3F;

            if !(trim && loc == 0) {
                trim = false;

                f.write_char(
                    BASE64_RADIX.chars()
                    .nth(loc as usize)
                    .expect("BASE64_RADIX is less than 64 characters long?!")
                )?;
            }
        }

        Ok(())
    }
}

impl TryFrom<&str> for Uid {
    type Error = &'static str;

    fn try_from(s: &str) -> Result<Uid, &'static str> {
        // decode base64
        let mut num = 0u64;

        // convert from base64
        for sh in s.chars() {
            num <<= 6;

            num |= match BASE64_RADIX.chars().position(|x| x == sh) {
                Some(n) => n,
                None => return Err("invalid formatted string"),
            } as u64;
        }

        Ok(Uid::new(num))
    }
}

impl TryFrom<String> for Uid {
    type Error = &'static str;

    fn try_from(s: String) -> Result<Uid, &'static str> {
        <Uid as TryFrom<&str>>::try_from(&s[..])
    }
}

/// A stateless "Universally Unique Identifier" generator.
///
/// This works very similarlly to Twitter's snowflake. In this case, 48 bytes
/// are set aside for the Leiko.io timestamp in milliseconds. Then, 8 bytes are
/// set aside for a randomly generated "nonce" using the `ChaCha20Rng`
/// algorithm. The next 4 bytes are the process's PID modulo 16. The next four
/// bytes are a sequenctial nonce. See the diagram below for more details.
///
/// A Leiko.io timestamp is how much milliseconds have passed since the
/// [`LEIKO_EPOCH`].
///
/// ```text
/// 0000000000000000000000000000000000000000110000111001111011010101
/// |             leiko timestamp (ms)             || rand ||  sq  |
/// ```
pub fn gen_uuid<R>(mut rng: R, nonce: u8) -> Uid
where R: RngCore {
    // get current time
    let timestamp = (SystemTime::now()
        .duration_since(UNIX_EPOCH).unwrap()
        .as_millis() - LEIKO_EPOCH) as u64;

    // generate random nonce
    let rand = {
        let mut rand = [0u8; 1];
        rng.fill_bytes(&mut rand);

        rand[0]
    };

    // put everything together!
    Uid((timestamp << 16)   |
        (rand as u64) << 8  |
        nonce as u64 & 0xFF)
}

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    use std::convert::TryInto as _;

    use rand::SeedableRng;
    
    use rand_chacha::ChaCha20Rng;

    #[test]
    fn test_serde_uid() {
        let uid = Uid::new(1024);
        
        let encoded = uid.to_string();

        let decoded: Uid = encoded.try_into().unwrap();

        assert_eq!(uid, decoded);
    }

    #[bench]
    fn bench_uuid_gen(b: &mut Bencher) {
        let mut rng = ChaCha20Rng::from_entropy();
        let mut nonce = 0u8;
        b.iter(move || { gen_uuid(&mut rng, nonce); nonce += 1; });
    }
}
