use sha2::{Sha256, Digest};

/// Characters that can be used to create a token.
pub const TOKEN_BAG: &str = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

/// Create a cryptographically secure token.
pub fn gen_secure_token<R>(mut rng: R, len: usize) -> String 
where R: rand::RngCore {
    let mut s = String::with_capacity(len);

    for _ in 0..len {
        s.push(TOKEN_BAG.chars()
            .nth(rng.next_u32() as usize % TOKEN_BAG.len()).unwrap());
    }

    s
}

/// Create a cryptographically secure salt.
pub fn gen_secure_salt<R>(mut rng: R) -> [u8; 16] 
where R: rand::RngCore {
    let mut salt = [0u8; 16];
    rng.fill_bytes(&mut salt);
    salt
}

/// Hashes a password with a given password and salt.
pub fn hash_password<P, S>(pass: P, salt: S) -> [u8; 32]
where P: AsRef<[u8]>, S: AsRef<[u8]> {
    let mut result = [0u8; 32];

    result.copy_from_slice(Sha256::new()
        .chain(pass.as_ref())
        .chain(salt.as_ref())
        .result()
        .as_slice());

    result
}

#[cfg(test)]
mod tests {
    use super::*;
    
    use rand::SeedableRng;

    use rand_chacha::ChaCha20Rng;

    #[test]
    fn test_hashing() {
        let mut rng = ChaCha20Rng::from_entropy();

        let salt_one = gen_secure_salt(&mut rng);
        let salt_two = gen_secure_salt(&mut rng);

        // create two identical hashes
        let equal_one = hash_password("password", salt_one);
        let equal_two = hash_password("password", salt_one);

        // create failure hashes
        let not_equal_salt = hash_password("password", salt_two);
        let not_equal_pass = hash_password("wordpass", salt_one);

        assert_eq!(equal_one, equal_two);
        assert_ne!(equal_one, not_equal_salt);
        assert_ne!(equal_one, not_equal_pass);
    }
}
