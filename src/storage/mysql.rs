//! MySQL adaptor module.
//!
//! The MySQL adaptor [`MySqlAccessor`] implements the `StorageAccess` trait,
//! so it can be used with the [`StorageManager`].
//!
//! # Schema
//! When `hash` or `salt` is null, it indicates a locked account.
//! 
//! ```sql
//! CREATE TABLE users (
//!     id BIGINT PRIMARY KEY,
//!     username VARCHAR(32) NOT NULL,
//!     tokens VARCHAR(512) NOT NULL DEFAULT '',
//!     hash BINARY(32),
//!     salt BINARY(16)
//! )
//! ```
//! 
//! ```sql
//! CREATE TABLE posts (
//!     id BIGINT PRIMARY KEY,
//!     filename VARCHAR(256) NOT NULL,
//!     owner_id BIGINT NOT NULL
//! )
//! ```
//!
//! [`StorageManager`]: ../manager/struct.StorageManager.html

extern crate mysql;

use std::error::Error;
use std::result;

use mysql::{prelude::Queryable as _, Conn, params, Row};

use super::*;
/// A MySQL database accessor.
pub struct MySqlAccessor {
    conn: Conn,
}

impl MySqlAccessor {
    /// Returns a MySqlAccessor from the provided connection url.
    pub fn new(url: &str) -> result::Result<MySqlAccessor, Box<dyn Error>> {
        Ok(MySqlAccessor {
            conn: Conn::new(url)?,
        })
    }

    fn get_tokens(&mut self, id: u64) -> Result<String> {
        match self.conn.exec_first::<Row, _, _>(
            "SELECT tokens FROM users WHERE id = :id",
            params!{ id }
        )? {
            Some(mut row) => Ok(row.take(0).unwrap_or(String::new())),
            None => Err(StorageAccessError::not_found("user not found")),
        }
    }
}

impl StorageAccess for MySqlAccessor {
    fn create_user(insert: InsertUser) -> Result<()> {
        todo!();
    }

    fn retrieve_user(selector: UserSelector) -> Result<UserMetadata> {
        todo!();
    }

    fn update_user(selector: UserSelector, update: UpdateUser) -> Result<()> {
        todo!();
    }

    fn delete_user(selector: UserSelector) -> Result<()> {
        todo!();
    }
}
