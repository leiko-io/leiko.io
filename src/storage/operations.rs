use super::structs::*;

use crate::uid::{Uid, self};
use crate::pass;

/// Insert a user row into a database.
///
/// # Guarantees
/// `username` will be 32 characters or less.
pub struct InsertUser<'a> {
    id: Uid,
    username: &'a str,
    hash_double: Option<([u8; 32], [u8; 16])>,
    tokens: Vec<ApiToken>,
}

impl<'a> InsertUser<'a> {
    /// Create a new user with id gen parameters.
    ///
    /// # Panics
    /// Panics if `username` is longer than 32 characters.
    pub fn new<R>(rng: R, nonce: u8, username: &'a str) -> InsertUser<'a> 
    where R: rand::RngCore {
        assert!(username.chars().count() <= 32);

        InsertUser {
            id: uid::gen_uuid(rng, nonce),
            username,
            hash_double: None,
            tokens: Vec::new(),
        }
    }

    /// Set a hash double (pass, hash) by hashing the password and generating
    /// a random salt.
    pub fn hash_pass<R>(mut self, password: &str, rng: R) -> InsertUser<'a> 
    where R: rand::RngCore {
        // generate a salt
        let salt = pass::gen_secure_salt(rng);

        // generate the hash
        let hash = pass::hash_password(password, &salt);

        self.hash_double = Some((hash, salt));
        self
    }

    /// Add a token to the insert query.
    pub fn add_token<R>(mut self, token: R) -> InsertUser<'a>
    where R: Into<ApiToken> {
        self.tokens.push(token.into());
        self
    }

    /// Get the id of the user.
    pub fn id(&self) -> Uid {
        self.id
    }

    /// Get the username of the user.
    pub fn username(&self) -> &str {
        &self.username
    }

    /// Get the hash double of the user.
    pub fn hash_double(&self) -> Option<(&[u8], &[u8])> {
        match self.hash_double {
            Some((ref hash, ref salt)) => Some((hash, salt)),
            None => None,
        }
    }

    /// Get a slice of the api tokens being attached to this user.
    pub fn api_tokens(&self) -> &[ApiToken] {
        &self.tokens
    }
}

/// Update a user.
pub struct UpdateUser<'a> {
    username: Option<&'a str>,
    hash_double: Option<([u8; 32], [u8; 16])>,
    tokens_add: Vec<ApiToken>,
    tokens_clear: bool,
}

impl<'a> UpdateUser<'a> {
    /// Get the updated username, if an update is wanted.
    pub fn username(&'a self) -> Option<&'a str> {
        self.username
    }

    /// Get the updated hash double, if an update is wanted.
    pub fn hash_double(&self) -> Option<(&[u8], &[u8])> {
        match self.hash_double {
            Some((ref hash, ref pass)) => Some((hash, pass)),
            None => None,
        }
    }
    
    /// Get tokens added.
    pub fn added_tokens(&self) -> &[ApiToken] {
        &self.tokens_add
    }

    pub fn invalidate_all_tokens(&self) -> bool {
        self.tokens_clear
    }
}

/// Select a user from a database.
///
/// This should represent a single unique user in the database.
pub enum UserSelector<'a> {
    Id(Uid),
    Username(&'a str),
}

impl<'a> UserSelector<'a> {
    /// Fetch a user by an id.
    pub fn id<I>(id: I) -> UserSelector<'a> 
    where I: Into<Uid> {
        UserSelector::Id(id.into())
    }

    /// Fetch a user by a username.
    pub fn username(username: &'a str) -> UserSelector<'a> {
        UserSelector::Username(username)
    }
}
