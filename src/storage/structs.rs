use std::fmt::{Display, Debug, Formatter, Error as FmtError};

use crate::pass;
use crate::uid::Uid;

/// API token scopes.
///
/// The `Display` implementation returns a human-readable version of the 
/// scopes. If you want to store Scope in a compressed string, 
/// [`Scope::compress()`] and [`Scope::decompress()`] do what you need.
///
/// *I'll make this into a macro later...*
#[derive(Clone, Copy, PartialEq, Eq)]
pub struct Scope(u32);

impl Scope {
    /// The `CREATE_POST` scope allows posts to be created on behalf of the
    /// owner of the API token.
    pub const CREATE_POST: Scope = Scope::new(0b0001);

    /// The `DELETE_POST` scope allows posts to be deleted on behalf of the
    /// owner of the API token.
    pub const DELETE_POST: Scope = Scope::new(0b0010);

    /// The wildcard scope.
    ///
    /// # Warning!
    /// This scope is different from "all scopes" where all of the scope bits
    /// are set. The "wildcard" scope means "every scope that will be created,"
    /// not just all scopes.
    pub const WILDCARD: Scope = Scope::wildcard();

    /// Create a scope from the internally handled `u32`.
    pub const fn new(scopes: u32) -> Scope {
        Scope(scopes)
    }

    /// Create a "wildcard" scope.
    ///
    /// Prefer using the associated const [`Scope::WILDCARD`] instead.
    ///
    /// # Warning!
    /// This scope is different from "all scopes" where all of the scope bits
    /// are set. The "wildcard" scope means "every scope that will be created,"
    /// not just all scopes.
    pub const fn wildcard() -> Scope {
        Scope(1 << (std::mem::size_of::<u32>()-1))
    }

    /// Returns `true` if `self` is a wildcard scope. Otherwise, returns
    /// `false`.
    pub fn is_wildcard(self) -> bool {
        self.0 >> (std::mem::size_of::<u32>()-1) > 0
    }

    /// Checks if one scope contains the permissions defined in another.
    pub fn contains(self, other: Scope) -> bool {
        if self.is_wildcard() { true }
        else { self.owner_bit(other, 0) && self.owner_bit(other, 1) }
    }

    /// Encodes the flags into single alphabetic characters.
    ///
    /// This works much better than inlining the internal base-10 number,
    /// as it intelligenly encodes the wildcard scope as "*".
    ///
    /// # Examples
    /// ```
    /// use leiko_io::storage::Scope;
    ///
    /// let scope = Scope::CREATE_POST;
    ///
    /// // encode the scope!
    /// let encoded = scope.compress();
    /// assert_eq!(encoded, "C");
    ///
    /// // decode the scope!
    /// let decoded = Scope::decompress(&encoded);
    /// assert_eq!(scope, decoded);
    /// ```
    /// 
    /// ```
    /// use leiko_io::storage::Scope;
    ///
    /// let scope = Scope::WILDCARD;
    ///
    /// // intelligently handle wildcards!
    /// let encoded = scope.compress();
    /// assert_eq!(encoded, "*");
    ///
    /// let decoded = Scope::decompress(&encoded);
    /// assert_eq!(scope, decoded);
    /// ```
    pub fn compress(self) -> String {
        // create a new string
        let mut buf = String::with_capacity(4);

        // check if this is a wildcard symbol
        if self.is_wildcard() {
            buf.push('*');
        } else {
            // map each flag to a symbol.
            if self.get_bit(0) { buf.push('C') }
            if self.get_bit(1) { buf.push('D') }
        }

        buf
    }

    /// Decodes the flags from a list of alphabetic characters.
    ///
    /// See [`Scope::compress()`] for more information.
    pub fn decompress(s: &str) -> Scope {
        if s == "*" {
            // wildcard!
            Scope::WILDCARD
        } else {
            let mut scope = Scope::new(0);

            for c in s.chars() {
                match c {
                    'C' => scope.set_bit(0, true),
                    'D' => scope.set_bit(1, true),
                    _ => (),
                }
            }

            scope
        }
    }
    
    //# BIT HANDLING FUNCTIONS
    fn set_bit(&mut self, ind: u32, on: bool) {
        assert!((ind as usize) < std::mem::size_of::<u32>());

        let mask = 1 << ind;

        if on {
            self.0 |= mask;
        } else {
            self.0 &= !mask;
        }
    }

    fn get_bit(&self, ind: u32) -> bool {
        assert!((ind as usize) < std::mem::size_of::<u32>());

        self.0 & (1 << ind) > 0
    }

    fn owner_bit(&self, other: Scope, ind: u32) -> bool {
        // in this case, `self` is the owner.
        self.get_bit(ind) || !other.get_bit(ind) 
    }
}

impl Display for Scope {
    fn fmt(&self, f: &mut Formatter) -> std::result::Result<(), FmtError> {
        <u32 as Display>::fmt(&self.0, f)
    }
}

impl Debug for Scope {
    fn fmt(&self, f: &mut Formatter) -> std::result::Result<(), FmtError> {
        <u32 as Debug>::fmt(&self.0, f)
    }
}

/// A fully-qualified API token.
///
/// Functions implemented on this struct expect `token` to be 32 characters
/// long. If `token` is any longer or any shorter, things may break or panic.
pub struct ApiToken {
    token: String,
    scopes: Scope,
}

impl ApiToken {
    /// Create a new API token from an RNG and a set of scopes.
    pub fn new<R>(rng: R, scopes: Scope) -> ApiToken 
    where R: rand::RngCore {
        ApiToken {
            token: pass::gen_secure_token(rng, 32),
            scopes,
        }
    }

    /// Create a new API token from internal pieces.
    pub fn from_internal<S>(token: S, scopes: Scope) -> ApiToken 
    where S: Into<String> {
        ApiToken {
            token: token.into(),
            scopes,
        }
    }

    /// Fetch the token.
    pub fn token(&self) -> &str {
        &self.token
    }

    /// Fetch the scopes this token is qualified to.
    pub fn scopes(&self) -> Scope {
        self.scopes
    }

    /// Build a token that can be sent over the wire.
    pub fn net_token(&self, id: Uid, f: &mut Formatter) -> Result<(), FmtError> {
        write!(f, "{}{}", id, self.token)
    }
}

// /// Post metadata.
//pub struct PostMetadata {
//    pub filename: String,
//    pub owner_id: u64,
//}

/// User metadata.
pub struct UserMetadata {
    id: Uid,
    username: String,
}

impl UserMetadata {
    /// Creates a raw representation of a user.
    pub fn new<I, U>(id: I, username: U) -> UserMetadata
    where U: Into<String>, I: Into<Uid> {
        UserMetadata { id: id.into(), username: username.into() }
    }

    /// Get the id of the represented user.
    pub fn id(&self) -> Uid {
        self.id
    }

    /// Get the username of the represented user.
    pub fn username(&self) -> &str {
        &self.username
    }
}

impl Display for UserMetadata {
    fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError> {
        f.write_str(&self.username)
    }
}
