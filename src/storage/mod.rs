#[cfg(feature = "mysql")]
pub mod mysql;

pub mod operations;
pub mod structs;

use operations::*;
use structs::*;

pub use structs::Scope;

use std::fmt::{Display, Debug, Formatter, Error as FmtError};

pub type Result<T> = std::result::Result<T, StorageAccessError>;

/// Describes an error that could be encountered when accessing a database.
#[derive(Clone, Copy)]
pub enum StorageAccessErrorKind {
    /// The model was not found in the database.
    NotFound,
    /// The user (or model) is currently locked.
    Locked,
    /// Some other internal error occured.
    Internal,
    /// An internal error that cannot be recovered from.
    Fatal,
}

/// An error returned by a `StorageAccess` implementor.
pub struct StorageAccessError {
    kind: StorageAccessErrorKind,
    msg: String,
}

impl StorageAccessError {
    /// Creates a new "not found" storage access error.
    pub fn not_found<S>(msg: S) -> StorageAccessError
    where S: Into<String> {
        StorageAccessError {
            kind: StorageAccessErrorKind::NotFound,
            msg: msg.into(),
        }
    }

    /// Creates a new "locked" storage access error.
    ///
    /// Locking a user persists the user's information in the database, but
    /// allows no changes to its internal state or allows actions to be taken
    /// on behalf of the user.
    pub fn locked<S>(msg: S) -> StorageAccessError
    where S: Into<String> {
        StorageAccessError {
            kind: StorageAccessErrorKind::Locked,
            msg: msg.into(),
        }
    }

    /// Creates a new "internal" storage access error.
    pub fn internal<S>(msg: S) -> StorageAccessError
    where S: Into<String> {
        StorageAccessError {
            kind: StorageAccessErrorKind::Internal,
            msg: msg.into(),
        }
    }

    /// Creates a new "fatal" storage access error.
    ///
    /// # Warning!
    /// Throwing this error to the Leiko.io backend will most likely result in
    /// a nasty error message and a termination of the program. This error is
    /// traditionally used to "stop for repairs" when a security fault is
    /// detected.
    pub fn fatal<S>(msg: S) -> StorageAccessError
    where S: Into<String> {
        StorageAccessError {
            kind: StorageAccessErrorKind::Fatal,
            msg: msg.into(),
        }
    }

    /// Gets the error message inside the error.
    pub fn message(&self) -> &str {
        &self.msg
    }

    /// Gets the error kind of the error.
    pub fn kind(&self) -> StorageAccessErrorKind {
        self.kind.clone()
    }
}

impl<T> From<T> for StorageAccessError
where T: std::error::Error {
    fn from(e: T) -> StorageAccessError {
        StorageAccessError::internal(e.to_string())
    }
}

impl Display for StorageAccessError {
    fn fmt(&self, f: &mut Formatter) -> std::result::Result<(), FmtError> {
        f.write_str(&self.msg)
    }
}

impl Debug for StorageAccessError {
    fn fmt(&self, f: &mut Formatter) -> std::result::Result<(), FmtError> {
        <Self as Display>::fmt(self, f)
    }
}

/// A database access manager.
///
/// The `StorageAccess` trait is supposed to be a simple abstraction to allow
/// flexibility to the implementors. Each implemented function is expected to
/// use all of the information passed to it.
pub trait StorageAccess {
    /// Create a new user in the database.
    fn create_user(insert: InsertUser) -> Result<()>;
    /// Retrieve a user in the database.
    ///
    /// If the user cannot be found, returns a [not found]
    /// error.
    ///
    /// [not found]: enum.StorageAccessErrorKind.html#variant.NotFound
    fn retrieve_user(selector: UserSelector) -> Result<UserMetadata>;
    /// Update a user in the database.
    ///
    /// If the user cannot be found, returns a [not found]
    /// error.
    ///
    /// [not found]: enum.StorageAccessErrorKind.html#variant.NotFound
    fn update_user(selector: UserSelector, update: UpdateUser) -> Result<()>;
    /// Delete a user in the database.
    ///
    /// If the user cannot be found, returns a [not found]
    /// error.
    ///
    /// [not found]: enum.StorageAccessErrorKind.html#variant.NotFound
    fn delete_user(selector: UserSelector) -> Result<()>;
}
