use chrono::prelude::Local;

#[cfg(unix)]
use std::os::unix::io::AsRawFd;

use log::{SetLoggerError, LevelFilter, Record, Level, Metadata};

pub const ANSI_ERROR: &str = "\x1B[1;31m";
pub const ANSI_WARN: &str = "\x1B[1;33m";
pub const ANSI_INFO: &str = "\x1B[1;36m";
pub const ANSI_DEBUG: &str = "\x1B[1;35m";
pub const ANSI_TRACE: &str = "\x1B[1;37m";
pub const ANSI_RESET: &str = "\x1B[0;37m";

#[cfg(unix)]
fn is_tty<F>(f: F) -> bool
where F: AsRawFd {
    unsafe { 
        libc::isatty(f.as_raw_fd()) == 1 
    }
}

pub struct MamoruLogger {
    accepted: Level,
    color_stdout: bool,
    color_stderr: bool,
}

impl MamoruLogger {
    /// Creates a `MamoruLogger` with default options.
    pub fn new() -> MamoruLogger {
        MamoruLogger {
            accepted: Level::Info,
            color_stdout: false,
            color_stderr: false,
        }
    }

    /// Sets the color mode for both `stdout` and `stderr`.
    pub fn color_mode(mut self, color_mode: bool) -> MamoruLogger {
        self.color_stdout = color_mode;
        self.color_stderr = color_mode;
        self
    }

    /// Auto-detects the proper color mode to be used.
    ///
    /// This function can identify the `stdout` and `stderr` streams as TTys.
    pub fn auto_color(mut self) -> MamoruLogger {
        self.color_stdout = is_tty(std::io::stdout());
        self.color_stderr = is_tty(std::io::stderr());
        self
    }

    /// Sets the accepted level.
    pub fn min_level(mut self, level: Level) -> MamoruLogger {
        self.accepted = level;
        self
    }
}

impl Default for MamoruLogger {
    fn default() -> MamoruLogger {
        MamoruLogger::new()
    }
}

impl log::Log for MamoruLogger {
    fn enabled(&self, metadata: &Metadata) -> bool {
        metadata.level() <= self.accepted
    }

    fn log(&self, record: &Record) {
        if self.enabled(record.metadata()) {
            // get a time
            let time = Local::now().time().format("%H:%M:%S");

            if record.level() <= Level::Error {
                // print to stderr
                if self.color_stderr {
                    eprintln!("{}[{} {:>5}] {}{}", ANSI_ERROR, time,
                        record.level(), record.args(), ANSI_RESET)
                } else {
                    eprintln!("[{} {:>5}] {}", time,
                        record.level(), record.args())
                }
            } else {
                // print to stdout
                if self.color_stdout {
                    println!("{}[{} {:>5}]{} {}",
                        match record.level() {
                            Level::Error => unreachable!(),
                            Level::Warn => ANSI_WARN,
                            Level::Info => ANSI_INFO,
                            Level::Debug => ANSI_DEBUG,
                            Level::Trace => ANSI_TRACE,
                        }, time, record.level(), ANSI_RESET, record.args())
                } else {
                    println!("[{} {:>5}] {}", time,
                        record.level(), record.args())
                }
            }
        }
    }

    fn flush(&self) {}
}

pub fn init() -> Result<(), SetLoggerError> {
    log::set_boxed_logger(Box::new(MamoruLogger::default()
        .auto_color()
        .min_level(Level::Info)))
        .map(|()| log::set_max_level(LevelFilter::Info))
}
